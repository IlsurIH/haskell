factorial :: Integer -> Integer
factorial 0 = 1
factorial n = n * factorial (n - 1)

fibonacci :: Integer -> Integer
fibonacci 0 = 1
fibonacci 1 = 1
fibonacci n = let 
        prev =  fibonacci (n-1)
        predPrev = fibonacci (n-2) 
            in prev + predPrev

ackerman :: Integer -> Integer -> Integer
ackerman m n = 
    if (m == 0)
    then n+1
    else if (n == 0)
          then 
            ackerman (m-1) 1
          else 
            ackerman (m-1) (ackerman m (n-1))