type Name = Char
	-- data for term
	data Term = Var Name | Abs Name Term | App Term Term deriving (Show, Eq)

	-- substituation function
	subs :: Term -> Term -> Name -> Term
	subs t (Var y) x = if x == y then t else Var y
	subs t (Abs y t1) x = if x == y 
				then y t1
				else Abs y (subs t t1 x)
	subs t (App t1 t2) x = App (subs t t1 x) (subs t t2 x)

	-- term eval function
	eval :: Term -> Term

	eval (Var x)     = Var x
	eval (Abs x t)   = Abs x (eval t)
	eval (App t1 t2) =  case t1 of
				Var n -> Var n
				Abs n t -> eval(subs t2 t n)
				App (Var x) t -> App t1 t2
				App t3 t4 -> eval (App (eval t1) t2)
