module Semestr2 where

	type Name = Char

	data Type = TInt | TBool | TArr Type Type | TError deriving (Show, Eq)

	type Context = [(Name, Type)]

	data Term = Var Name | Abs Name Type Term | App Term Term | Lit Ground deriving (Show, Eq)

	data Ground = LInt | LBool deriving (Show, Eq)

	extend :: Context -> (Name, Type) -> Context
	extend c xt = xt:c

	checkTerm :: Context -> Term -> Type
	checkTerm _ (Lit a) = case a of
				LInt -> TInt
				LBool -> TBool
	checkTerm c (Var x) = case (lookup x c) of
				Just e -> e
				Nothing ->  TError
	checkTerm c (Abs n ty te) = case ty of
					TError -> TError
					_ -> TArr ty (checkTerm (extend c (n, ty)) te)
	checkTerm c (App t1 t2) = if ((checkTerm c t1) == TError || (checkTerm c t2) == TError)
								then TError
							  else
								checkTArr (checkTerm c t1) (checkTerm c t2)

	checkTArr :: Type -> Type -> Type
	checkTArr _ _ = TError
	checkTArr (TArr t1 t2) t = if t1 == t2 then t1 else TError

	check :: Type -> Bool
	check t = case t of
			TError -> False
			_ -> True