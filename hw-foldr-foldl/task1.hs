mapr' :: (a -> b) -> [a] -> [b]
mapr' _ [] = []
mapr' f (x:xs)   = f x : foldr (\y ys -> (f y):ys) [] xs

mapl' :: (a -> b) -> [a] -> [b]
mapl' _ [] = []
mapl' f (x:xs)   = foldl (\ys y -> (f y):ys) [f x] xs