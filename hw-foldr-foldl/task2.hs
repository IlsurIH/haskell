filter' :: (a -> Bool) -> [a] -> [a]
filter' f [] = []
filter' f (x:xs) = if f x then (x: filter' f xs) else filter' f xs

filterFoldl :: (a -> Bool) -> [a] -> [a]
filterFoldl f [] = []
filterFoldl f (x:xs) = foldl (\ys y -> if (f y) then y:ys else ys) [] xs

filterFoldr :: (a -> Bool) -> [a] -> [a]
filterFoldr f [] = []
filterFoldr f (x:xs) = foldr (\y ys -> if (f y) then y:ys else ys) [] xs

concat' :: [[a]] -> [a]
concat' [] = []
concat' (x:xs) =  x ++ concat' xs

concatFoldr :: [[a]] -> [a]
concatFoldr [] = []
concatFoldr (x:xs) =  foldl (\x y -> x ++ y) x xs

concatFoldl :: [[a]] -> [a]
concatFoldl [] = []
concatFoldl (x:xs) =  foldr (\x y -> x ++ y) x xs

concatMap' :: (a -> [b]) -> [a] -> [b]
concatMap' f [] = []
concatMap' f (x:xs) =  f x ++ concatMap' f xs

concatMapFoldr :: (Int -> [b]) -> [Int] -> [b]
concatMapFoldr f [] = []
concatMapFoldr f l =  foldr (\x y -> (f x) ++ y) [] l

concatMapFoldl :: (Int -> [b]) -> [Int] -> [b]
concatMapFoldl f [] = []
concatMapFoldl f l =  foldl (\y x -> (f x) ++ y) [] l

--main = do ( print $ concatMapFoldl (replicate 5) [1..5])
