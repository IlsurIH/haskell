module Main where

--Tailed recursion of factorial
factorialTail :: Integer -> Integer -> Integer
factorialTail result index = if index < 1 then result else factorialTail (result * index) (index - 1)
factorial i = factorialTail 1 i
--main = print ( factorial 5)

--Tailed recursion of fibonacci (zero based)

fibonacciTail :: Integer -> Integer -> Integer -> Integer
fibonacciTail i f s = if i < 1 then (f + s) else fibonacciTail (i-1) s (s+f)

fibonacci :: Integer -> Integer
fibonacci i = fibonacciTail  (i - 1) 0 1
main = print ( fibonacci 5)